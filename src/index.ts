// import des composants necessaire a la creation de l'api
import express from 'express'
import cors from 'cors'
import bodyParser from 'body-parser'
import helmet from 'helmet'
import useragent from 'express-useragent'
import { Define, Cdg, color } from './utils'
import dotenv from 'dotenv'
import { setRoutes } from './routes'
//import './db/synchronizationDB'
dotenv.config()
const app = express()

app.use(useragent.express())
app.use(helmet())

app.use(cors())
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

app.use((err : any, req : any, res : any, next : any) => {
    if (err && err.status === 400 && 'body' in err) {
        return Cdg.api(res, new Promise((resolve) => {
            resolve({
                status: Define.badRequestErrorCode,
                message: 'Votre requête contient une ou plusieurs erreurs',
                data: [err.type]
            })
        }))
    }

    next()
})
//CREER LES ROUTES QU UNE FOIS QUE LE SERVEUR SE LANCE
setRoutes(app)

app.use((_err : any, res : any) => {
    return Cdg.api(res, new Promise((resolve) => {
        resolve({status: Define.notfoundErrorCode, message: 'Route introuvable ou inexistante', data: []})
    }));
});

const port = process.env.PORT_API || 5000
app.listen(port, ()=>{
    console.info(color.BgCyan,`APP en cours d'utilisation sur le port :: ${port}`)
}).on('error', (error: Error)=>{
    //@ts-ignore
    if(error.code == 'EADDRINUSE')//@ts-ignore
        console.error(color.BgRed,`port déjà en cours d'utilisation :: ${error.code}`) 
    else //@ts-ignore
        console.error(color.BgRed,`erreur inconnu s'est produite ${error.code}`)
})