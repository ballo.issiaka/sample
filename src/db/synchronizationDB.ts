import {
    CandidatConcoursModel, CandidatDiplomeModel, CandidatFonctionnaireModel, CandidatModel,
    ConcoursModel, ConcoursTypeConcoursModel, TypeConcoursModel,
    DiplomeModel, EcoleModel, NiveauModel, TypeEnseignementModel,
    EmploiModel, FamilleEmploiModel,
    FonctionnaireModel, FonctionnaireDiplomeModel, FonctionModel,
} from "../models/";

import connexion from './connexionDB';

// Les relations
FonctionModel.hasOne(FonctionnaireModel, { foreignKey: 'id_fonction' });
CandidatModel.belongsToMany(FonctionnaireModel, {through: CandidatFonctionnaireModel });
FonctionnaireModel.belongsToMany(CandidatModel, {through: CandidatFonctionnaireModel });
CandidatModel.belongsToMany(ConcoursModel, {through: CandidatConcoursModel });
ConcoursModel.belongsToMany(CandidatModel, {through: CandidatConcoursModel });
ConcoursModel.belongsToMany(TypeConcoursModel, {through: ConcoursTypeConcoursModel });
TypeConcoursModel.belongsToMany(ConcoursModel, {through: ConcoursTypeConcoursModel });
CandidatModel.belongsToMany(DiplomeModel, {through: CandidatDiplomeModel });
DiplomeModel.belongsToMany(CandidatModel, {through: CandidatDiplomeModel });
FonctionnaireModel.belongsToMany(DiplomeModel, {through: FonctionnaireDiplomeModel });
DiplomeModel.belongsToMany(FonctionnaireModel, {through: FonctionnaireDiplomeModel });
FamilleEmploiModel.hasOne(ConcoursModel, { foreignKey: 'id_famille_emploi' });
FamilleEmploiModel.hasMany(EmploiModel, { foreignKey: 'id_famille_emploi' });
TypeEnseignementModel.hasOne(DiplomeModel, {foreignKey: 'id_type_enseignement'});
NiveauModel.hasOne(DiplomeModel, {foreignKey: 'id_niveau'});
EcoleModel.hasOne(DiplomeModel, { foreignKey: 'id_ecole' });



// Créations des modèles dans notre Base de Données.
connexion.sync({ force: true })
    .then((results:any) => {
        // console.log(results);
        console.log('Tous les modèles ont été synchronisés avec succès');
        console.log(`Server listening at: http://localhost:3000`);
    })
    .catch((error:any) => {
        console.log('Impossible de synchroniser les modèles à notre Base de Données: ', error.message);
    });