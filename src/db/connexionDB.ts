import {Sequelize} from'sequelize';
import mysql from "mysql2/promise";

let connexion:any;

if (process.env.NODE_ENV==='production') {
    connexion = new Sequelize('database','username','password', {
        host: 'localhost',
        dialect: 'mysql',
        logging: false
    });
}
else {
    connexion = new Sequelize('inscription','root','', {
        host: 'localhost',
        dialect: 'mysql',
        logging: false
    });
}

(async()=>{
    await mysql.createConnection({
        user : "root",
        host : "localhost",
    }).then((connexion) => {
        connexion.query(`CREATE DATABASE IF NOT EXISTS inscription;`) 
            .then(() => {
                console.log('Base de données crée avec succès');
            });
    });
})()

export default connexion;