import { DataTypes } from 'sequelize';
import connexion from '../db/connexionDB.js';

export const ConcoursTypeConcoursModel = connexion.define('concours_typeconcours', {
    date: {
        type: DataTypes.DATE
    }
});