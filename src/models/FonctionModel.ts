import { DataTypes } from 'sequelize';
import connexion from '../db/connexionDB.js';

export const FonctionModel = connexion.define('fonction', {
    id_fonction: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    libelle_fonction: {
        type: DataTypes.STRING,
        allowNull: false,
    },
});