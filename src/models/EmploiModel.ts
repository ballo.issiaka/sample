import { DataTypes } from 'sequelize';
import connexion from '../db/connexionDB.js';

export const EmploiModel = connexion.define('emploi', {
    id_emploi: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    libelle_emploi: {
        type: DataTypes.STRING,
        allowNull: false,
    },
});