import { DataTypes } from 'sequelize';
import connexion from '../db/connexionDB';

export const ConcoursModel = connexion.define('concours', {
    id_concours: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    libelle_concours: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    code_concours: {
        type: DataTypes.STRING,
        allowNull: false,
    }
});