import { DataTypes } from 'sequelize';
import connexion from '../db/connexionDB.js';

export const CandidatDiplomeModel = connexion.define('candidat_diplome', {
    date: {
        type: DataTypes.STRING
    }
});