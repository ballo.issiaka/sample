import { DataTypes } from 'sequelize';
import connexion from '../db/connexionDB.js';

export const DiplomeModel = connexion.define('diplome', {
    id_diplome: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    numero_diplome: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    libelle_diplome: {
        type: DataTypes.STRING,
        allowNull: false,
    },
});