import { DataTypes } from 'sequelize';
import connexion from '../db/connexionDB.js';

export const CandidatFonctionnaireModel = connexion.define('candidats_fonctionnaire', {
    date: {
        type: DataTypes.DATE
    }
});