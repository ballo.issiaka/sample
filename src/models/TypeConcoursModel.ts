import { DataTypes } from 'sequelize';
import connexion from '../db/connexionDB.js';

export const TypeConcoursModel = connexion.define('types_concours', {
    id_type_concours: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    libelle_type_concours: {
        type: DataTypes.STRING,
        allowNull: false,
    },
});