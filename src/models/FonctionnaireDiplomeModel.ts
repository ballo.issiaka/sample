import { DataTypes } from 'sequelize';
import connexion from '../db/connexionDB.js';

export const FonctionnaireDiplomeModel = connexion.define('fonctionnaire_diplome', {
    date: {
        type: DataTypes.DATE
    }
});