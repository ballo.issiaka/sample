import { DataTypes } from 'sequelize';
import connexion from '../db/connexionDB.js';

export const NiveauModel = connexion.define('niveau_diplome', {
    id_niveau: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    libelle_niveau: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    cote_niveau: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
});