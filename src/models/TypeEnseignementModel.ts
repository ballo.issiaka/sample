import { DataTypes } from 'sequelize';
import connexion from '../db/connexionDB.js';

export const TypeEnseignementModel = connexion.define('types_enseignement', {
    id_type_enseignement: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    libelle_enseignement: {
        type: DataTypes.STRING,
        allowNull: false,
    },
});