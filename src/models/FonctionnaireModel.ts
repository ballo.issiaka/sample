import { DataTypes } from 'sequelize';
import connexion from '../db/connexionDB.js';

export const FonctionnaireModel = connexion.define('fonctionnaire', {
    id_fonctionnaire: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    genre: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    civilite: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    situation_matrimoniale: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    nom: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    prenom: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    telephone1: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    telephone2: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    date_naissance: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    lieu_naissance: {
        type: DataTypes.STRING
    },
    nombre_enfant: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    adresse: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    type_piece: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    numero_piece: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    },
    matricule: {
        type: DataTypes.STRING,
        unique: true
    },
    date_prise_service: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    photo: {
        type: DataTypes.BLOB,
        allowNull: true,
    }
});