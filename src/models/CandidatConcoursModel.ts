import { DataTypes } from 'sequelize';
import connexion from '../db/connexionDB.js';


export const CandidatConcoursModel = connexion.define('candidats_concours', {
    date: {
        type: DataTypes.DATE
    }
});