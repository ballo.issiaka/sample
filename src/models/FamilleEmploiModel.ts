import { DataTypes } from 'sequelize';
import connexion from '../db/connexionDB.js';

export const FamilleEmploiModel = connexion.define('famille_emploi', {
    id_famille_emploi: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },
    libelle_famille_emploi: {
        type: DataTypes.STRING,
        allowNull: false,
    },
});