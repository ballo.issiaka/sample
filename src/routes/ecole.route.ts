import { Cdg, color } from '../utils';
import {body, param} from 'express-validator';
import express, {Router, Response, NextFunction} from 'express';
import {routeDecorator} from './decorators.route';
import { EcoleController } from '../http/controllers';
import {Validator} from "../http/midlewares";

const route = Router();
(req : any, res : Response, next : NextFunction) => {
    return req.lang
};

route.post('/ecole/save',
    body("libelle_ecole").not().isEmpty().withMessage("le libellé de l'école est requis"),
    Validator.validate,
    (req:any, res:any)=>{
    Cdg.api(res, EcoleController.save(req.body))
})
route.get('/ecole/all',
    (req:any,res:any)=>{
        Cdg.api(res, EcoleController.allEcole())
})
route.get('/ecole/:id_ecole',
    param("id_ecole").isNumeric().withMessage("l'ID de l'école est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, EcoleController.findById(req.params.id_ecole))
})
route.delete('/ecole/:id_ecole',
    param("id_ecole").isNumeric().withMessage("l'ID de l'école est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, EcoleController.delete(req.params.id_ecole))
})
route.put('/ecole',
    body("id").isNumeric().withMessage("l'id de l'école est requis"),
    body("libelle_ecole").not().isEmpty().withMessage("le libellé de l'école est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, EcoleController.update(req.body))
})

export class EcoleRoute {
    @routeDecorator(route)
    static router : any
    constructor() {}
}