import { Cdg, color } from '../utils';
import {body, param} from 'express-validator';
import express, {Router, Response, NextFunction} from 'express';
import {routeDecorator} from './decorators.route';
import { DiplomeController } from '../http/controllers';
import {Validator} from "../http/midlewares";

const route = Router();
(req : any, res : Response, next : NextFunction) => {
    return req.lang
};

route.post('/diplome/save',
    body("numero_diplome").not().isEmpty().withMessage("le numéro du diplôme est requis"),
    body("libelle_diplome").not().isEmpty().withMessage("le libellé du diplôme est requis"),
    Validator.validate,
    (req:any, res:any)=>{
    Cdg.api(res, DiplomeController.save(req.body))
})
route.get('/diplome/all',
    (req:any,res:any)=>{
        Cdg.api(res, DiplomeController.allDiplome())
})
route.get('/diplome/:id_diplome',
    param("id_diplome").isNumeric().withMessage("l'ID du diplôme est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, DiplomeController.findById(req.params.id_diplome))
})
route.delete('/diplome/:id_diplome',
    param("id_diplome").isNumeric().withMessage("l'ID du diplôme est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, DiplomeController.delete(req.params.id_diplome))
})
route.put('/diplome',
    body("id").isNumeric().withMessage("l'id du diplôme est requis"),
    body("numero_diplome").not().isEmpty().withMessage("le numéro du diplôme est requis"),
    body("libelle_diplome").not().isEmpty().withMessage("le libellé du diplôme est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, DiplomeController.update(req.body))
})

export class DiplomeRoute {
    @routeDecorator(route)
    static router : any
    constructor() {}
}