import { Cdg, color } from '../utils';
import {body, param} from 'express-validator';
import express, {Router, Response, NextFunction} from 'express';
import {routeDecorator} from './decorators.route';
import { TypeEnseignementController } from '../http/controllers';
import {Validator} from "../http/midlewares";


const route = Router();
(req : any, res : Response, next : NextFunction) => {
    return req.lang
};

route.post('/typeenseignement/save',
    body("libelle_enseignement").not().isEmpty().withMessage("le libellé du Type d'Enseignement est requis"),
    Validator.validate,
    (req:any, res:any)=>{
    Cdg.api(res, TypeEnseignementController.save(req.body))
})
route.get('/typeenseignement/all',
    (req:any,res:any)=>{
        Cdg.api(res, TypeEnseignementController.allTypeEnseignement())
})
route.get('/typeenseignement/:id_type_enseignement',
    param("id_type_enseignement").isNumeric().withMessage("l'ID du Type d'Enseignement est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, TypeEnseignementController.findById(req.params.id_type_enseignement))
})
route.delete('/typeenseignement/:id_type_enseignement',
    param("id_type_enseignement").isNumeric().withMessage("l'ID du type d'Enseignement est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, TypeEnseignementController.delete(req.params.id_type_enseignement))
})
route.put('/typeenseignement',
    body("id").isNumeric().withMessage("l'ID du type concours est requis"),
    body("libelle_enseignement").not().isEmpty().withMessage("le libellé du type d'Enseignement est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, TypeEnseignementController.update(req.body))
})

export class TypeEnseignementRoute {
    @routeDecorator(route)
    static router : any
    constructor() {}
}