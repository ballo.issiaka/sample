import { Cdg, color } from '../utils';
import {body, param} from 'express-validator';
import express, {Router, Response, NextFunction} from 'express';
import {routeDecorator} from './decorators.route';
import { TypeConcoursController } from '../http/controllers';
import {Validator} from "../http/midlewares";


const route = Router();
(req : any, res : Response, next : NextFunction) => {
    return req.lang
};

route.post('/typeconcours/save',
    body("libelle_type_concours").not().isEmpty().withMessage("le libellé du type concours est requis"),
    Validator.validate,
    (req:any, res:any)=>{
    Cdg.api(res, TypeConcoursController.save(req.body))
})
route.get('/typeconcours/all',
    (req:any,res:any)=>{
        Cdg.api(res, TypeConcoursController.allTypeConcours())
})
route.get('/typeconcours/single/:id_type_concours',
    param("id_type_concours").isNumeric().withMessage("l'ID du type concours est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, TypeConcoursController.findById(req.params.id_type_concours))
})
route.delete('/typeconcours/remove/:id_type_concours',
    param("id_type_concours").isNumeric().withMessage("l'ID du type concours est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, TypeConcoursController.delete(req.params.id_type_concours))
})
route.put('/typeconcours/edit',
    body("id").isNumeric().withMessage("l'ID du type concours est requis"),
    body("libelle_type_concours").not().isEmpty().withMessage("le libellé du type concours est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, TypeConcoursController.update(req.body))
})

export class TypeConcoursRoute {
    @routeDecorator(route)
    static router : any
    constructor() {}
}