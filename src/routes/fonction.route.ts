import { Cdg, color } from '../utils';
import {body, param} from 'express-validator';
import express, {Router, Response, NextFunction} from 'express';
import {routeDecorator} from './decorators.route';
import { FonctionController } from '../http/controllers';
import {Validator} from "../http/midlewares";

const route = Router();
(req : any, res : Response, next : NextFunction) => {
    return req.lang
};

route.post('/fonction/save',
    body("libelle_fonction").not().isEmpty().withMessage("le libellé de la fonction est requis"),
    Validator.validate,
    (req:any, res:any)=>{
    Cdg.api(res, FonctionController.save(req.body))
})
route.get('/fonction/all',
    (req:any,res:any)=>{
        Cdg.api(res, FonctionController.allFonction())
})
route.get('/fonction/:id_fonction',
    param("id_fonction").isNumeric().withMessage("l'ID de la fonction est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, FonctionController.findById(req.params.id_fonction))
})
route.delete('/fonction/:id_fonction',
    param("id_fonction").isNumeric().withMessage("l'ID de la fonction est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, FonctionController.delete(req.params.id_fonction))
})
route.put('/fonction',
    body("id").isNumeric().withMessage("l'ID de la fonction est requis"),
    body("libelle_fonction").not().isEmpty().withMessage("le libellé de la fonction est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, FonctionController.update(req.body))
})

export class FonctionRoute {
    @routeDecorator(route)
    static router : any
    constructor() {}
}