import { Cdg, color } from '../utils';
import {body, param} from 'express-validator';
import express, {Router, Response, NextFunction} from 'express';
import {routeDecorator} from './decorators.route';
import { CandidatController } from '../http/controllers';
import {Validator, AuthenticationHlp} from "../http/midlewares";

const route = Router();
(req : any, res : Response, next : NextFunction) => {
    return req.lang
};

route.post('/candidat/save',
    body("numero_inscription").not().isEmpty().withMessage("le numéro d'inscription du candidat est requis"),
    body("genre").not().isEmpty().withMessage("le genre du candidat est requis"),
    body("login").not().isEmpty().withMessage("un login est requis"),
    body("password").not().isEmpty().withMessage("vous devez définir un mot de passe"),
    body("civilite").not().isEmpty().withMessage("la civilité du candidat est requis"),
    body("situation_matrimoniale").not().isEmpty().withMessage("la situation matrimoniale du candidat est requis"),
    body("nom").not().isEmpty().withMessage("le nom du candidat est requis"),
    body("prenom").not().isEmpty().withMessage("le prénom du candidat est requis"),
    body("telephone1").not().isEmpty().withMessage("le 1er numéro téléphone du candidat est requis"),
    body("telephone2").not().isEmpty().withMessage("le 2ème numéro téléphone du candidat est requis"),
    body("date_naissance").not().isEmpty().withMessage("la date de naissance du candidat est requis"),
    body("lieu_naissance").not().isEmpty().withMessage("le lieu de naissance du candidat est requis"),
    body("nombre_enfant").not().isEmpty().withMessage("le nombre d\'enfant du candidat est requis"),
    body("adresse").not().isEmpty().withMessage("l\'adresse du candidat est requis"),
    body("type_piece").not().isEmpty().withMessage("le type de pièce du candidat est requis"),
    body("numero_piece").not().isEmpty().withMessage("le numéro de la pièce du candidat est requis"),
    body("email").not().isEmpty().withMessage("l\'email du candidat est requis"),
    body("nom_pere").not().isEmpty().withMessage("le nom du père du candidat est requis"),
    body("nom_mere").not().isEmpty().withMessage("le nom de la mère du candidat est requis"),
    body("photo").not().isEmpty().withMessage("la photo du candidat est requis"),
    Validator.validate,
    (req:any, res:any)=>{
        Cdg.api(res, CandidatController.save(req.body))
})

route.get('/pdf/test',
    (req:any, res:any)=>{
        Cdg.api(res, CandidatController.pdfTest())
    })

route.post('/candidat/addDiplome',
    body('candidat').isNumeric().isLength({min: 1,max: Number.POSITIVE_INFINITY}).withMessage("l'id du candidat est requis"),
    body('diplome').isNumeric().isLength({min: 1,max: Number.POSITIVE_INFINITY}).withMessage("l'id du diplome est requis"),
    body('date').not().isString().withMessage("l'année d'obtention est requise"),
    Validator.validate,
    (req:any, res:any)=>{
        Cdg.api(res, CandidatController.addDiplome(req.body))
    })

route.get('/candidat/all',
    (req:any,res:any)=>{
        Cdg.api(res, CandidatController.allCandidats())
    })
route.get('/candidat/:id_candidat',
    param("id_candidat").isNumeric().withMessage("l'id du candidat est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, CandidatController.findById(req.params.id_candidat))
})
route.delete('/candidat/:id_candidat',
    param("id_candidat").isNumeric().withMessage("l'id du candidat est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, CandidatController.delete(req.params.id_candidat))
})
route.put('/candidat',
    body("numero_inscription").not().isEmpty().withMessage("le numéro d'inscription du candidat est requis"),
    body("genre").not().isEmpty().withMessage("le genre du candidat est requis"),
    body("civilite").not().isEmpty().withMessage("la civilité du candidat est requis"),
    body("situation_matrimoniale").not().isEmpty().withMessage("la situation matrimoniale du candidat est requis"),
    body("nom").not().isEmpty().withMessage("le nom du candidat est requis"),
    body("prenom").not().isEmpty().withMessage("le prénom du candidat est requis"),
    body("telephone1").not().isEmpty().withMessage("le 1er numéro téléphone du candidat est requis"),
    body("telephone2").not().isEmpty().withMessage("le 2ème numéro téléphone du candidat est requis"),
    body("date_naissance").not().isEmpty().withMessage("la date de naissance du candidat est requis"),
    body("lieu_naissance").not().isEmpty().withMessage("le lieu de naissance du candidat est requis"),
    body("nombre_enfant").not().isEmpty().withMessage("le nombre d\'enfant du candidat est requis"),
    body("adresse").not().isEmpty().withMessage("l\'adresse du candidat est requis"),
    body("type_piece").not().isEmpty().withMessage("le type de pièce du candidat est requis"),
    body("numero_piece").not().isEmpty().withMessage("le numéro de la pièce du candidat est requis"),
    body("email").not().isEmpty().withMessage("l\'email du candidat est requis"),
    body("nom_pere").not().isEmpty().withMessage("le nom du père du candidat est requis"),
    body("nom_mere").not().isEmpty().withMessage("le nom de la mère du candidat est requis"),
    body("photo").not().isEmpty().withMessage("la photo du candidat est requis"),
    AuthenticationHlp.verifyAuth,
    Validator.validate,
    (req:any,res:any)=>{
    console.log(req.user)
    let data = {login: req.user.login, ...req.body}
        Cdg.api(res, CandidatController.update(data))
})
route.post('/candidat/auth',
    body("login").not().isEmpty().withMessage("Veuillez entrer un login"),
    body("password").not().isEmpty().withMessage("Veuillez renseigner le mot de passe"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, CandidatController.auth(req.body))
    })

export class CandidatRoute {
    @routeDecorator(route)
    static router : any
    constructor() {}
}