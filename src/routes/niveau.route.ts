import { Cdg, color } from '../utils';
import {body, param} from 'express-validator';
import express, {Router, Response, NextFunction} from 'express';
import {routeDecorator} from './decorators.route';
import { NiveauController } from '../http/controllers';
import {Validator} from "../http/midlewares";

const route = Router();
(req : any, res : Response, next : NextFunction) => {
    return req.lang
};

route.post('/niveau/save',
    body("libelle_niveau").not().isEmpty().withMessage("le libellé du niveau diplôme est requis"),
    body("cote_niveau").not().isEmpty().withMessage("le code du niveau diplôme est requis"),
    Validator.validate,
    (req:any, res:any)=>{
    Cdg.api(res, NiveauController.save(req.body))
})
route.get('/niveau/all',
    (req:any,res:any)=>{
        Cdg.api(res, NiveauController.allNiveau())
})
route.get('/niveau/:id_niveau',
    param("id_niveau").isNumeric().withMessage("l'ID du niveau diplôme est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, NiveauController.findById(req.params.id_niveau))
})
route.delete('/niveau/:id_niveau',
    param("id_niveau").isNumeric().withMessage("l'ID du niveau diplôme est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, NiveauController.delete(req.params.id_niveau))
})
route.put('/niveau',
    body("id").isNumeric().withMessage("l'ID du niveau diplôme est requis"),
    body("libelle_niveau").not().isEmpty().withMessage("le libellé du niveau diplôme est requis"),
    body("cote_niveau").not().isEmpty().withMessage("le code du niveau diplôme est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, NiveauController.update(req.body))
})

export class NiveauRoute {
    @routeDecorator(route)
    static router : any
    constructor() {}
}