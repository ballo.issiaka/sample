import { Cdg, color } from '../utils';
import {body, param} from 'express-validator';
import express, {Router, Response, NextFunction} from 'express';
import {routeDecorator} from './decorators.route';
import { FamilleEmploiController } from '../http/controllers';
import {Validator} from "../http/midlewares";

const idRequiredText = 'L\'ID est requis'

const route = Router();
(req : any, res : Response, next : NextFunction) => {
    return req.lang
};

route.post('/famille-emploi/save',
    body("libelle_famille_emploi").not().isEmpty().withMessage("le libellé est requis"),
    Validator.validate,
    (req:any, res:any)=>{
    Cdg.api(res, FamilleEmploiController.save(req.body))
})
route.get('/famille-emploi/all',
    (req:any,res:any)=>{
        Cdg.api(res, FamilleEmploiController.all())
})
route.get('/famille-emploi/:id_famille_emploi',
    param("id_famille_emploi").isNumeric().withMessage(idRequiredText),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, FamilleEmploiController.findById(req.params.id_famille_emploi))
})
route.delete('/famille-emploi/:id_famille_emploi',
    param("id_famille_emploi").isNumeric().withMessage(idRequiredText),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, FamilleEmploiController.delete(req.params.id_famille_emploi))
})
route.put('/famille-emploi/edit',
    body("id").isNumeric().withMessage(idRequiredText),
    body("libelle_famille_emploi").not().isEmpty().withMessage("le libellé de l'école est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, FamilleEmploiController.update(req.body))
})

export class FamilleEmploiRoute {
    @routeDecorator(route)
    static router : any
    constructor() {}
}