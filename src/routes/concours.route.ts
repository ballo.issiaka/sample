import { Cdg, color } from '../utils';
import {body, param} from 'express-validator';
import express, {Router, Response, NextFunction} from 'express';
import {routeDecorator} from './decorators.route';
import { ConcoursController } from '../http/controllers';
import {Validator} from "../http/midlewares";

const route = Router();
(req : any, res : Response, next : NextFunction) => {
    return req.lang
};

route.post('/concours/save',
    body("libelle_concours").not().isEmpty().withMessage("le libellé du concours est requis"),
    body("code_concours").not().isEmpty().withMessage("le code du concours est requis"),
    Validator.validate,
    (req:any, res:any)=>{
    Cdg.api(res, ConcoursController.save(req.body))
})
route.get('/concours/all',
    (req:any,res:any)=>{
        Cdg.api(res, ConcoursController.allConcours())
})
route.get('/concours/:id_concours',
    param("id_concours").isNumeric().withMessage("l'id du concours est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, ConcoursController.findById(req.params.id_concours))
})
route.delete('/concours/:id_concours',
    param("id_concours").isNumeric().withMessage("l'id du concours est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, ConcoursController.delete(req.params.id_concours))
})
route.put('/concours/edit',
    body("id").isNumeric().withMessage("l'id du concours est requis"),
    body("libelle_concours").not().isEmpty().withMessage("le libellé du concours est requis"),
    body("code_concours").not().isEmpty().withMessage("le code du concours est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, ConcoursController.update(req.body))
})

export class ConcoursRoute {
    @routeDecorator(route)
    static router : any
    constructor() {}
}