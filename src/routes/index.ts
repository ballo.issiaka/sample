//SETROUTE EXISTE DANS LA CLASS DECORATOR
export * from './decorators.route'

// DECLARATION DES ROUTES DU PROJET
export * from './main.route'
export * from './concours.route'
export * from './candidat.route'
export * from './diplome.route'
export * from './ecole.route'
export * from './emploi.route'
export * from './fonction.route'
export * from './fonctionnaire.route'
export * from './niveau.route'
export * from './typeconcours.route'
export * from './typeenseignement.route'
export * from './famille-emploi.route'