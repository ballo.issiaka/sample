import { Cdg, color } from '../utils';
import {Router, Response, NextFunction} from 'express';
import {routeDecorator} from './decorators.route';

const route = Router();
(req : any, res : Response, next : NextFunction) => {
    return req.lang
};

route.get('/test', (req : any, res : any) => {
    let Q = new Promise((resolve, reject) => {
        console.info(color.BgGreen,"test route is working")
        resolve({status: 201, message: 'Service is Up', data: 'version:1.0.1'});
    })
    return Cdg.api(res, Q);
});
export class MainRoute {
    @routeDecorator(route)
    static router : any
    constructor() {}
}