import { Cdg, color } from '../utils';
import {body, param} from 'express-validator';
import express, {Router, Response, NextFunction} from 'express';
import {routeDecorator} from './decorators.route';
import { FonctionnaireController } from '../http/controllers';
import {Validator} from "../http/midlewares";

const route = Router();
(req : any, res : Response, next : NextFunction) => {
    return req.lang
};

route.post('/fonctionnaire/save',
    body("genre").not().isEmpty().withMessage("le genre du candidat est requis"),
    body("civilite").not().isEmpty().withMessage("la civilité du candidat est requis"),
    body("situation_matrimoniale").not().isEmpty().withMessage("la situation matrimoniale du candidat est requis"),
    body("nom").not().isEmpty().withMessage("le nom du candidat est requis"),
    body("prenom").not().isEmpty().withMessage("le prénom du candidat est requis"),
    body("telephone1").not().isEmpty().withMessage("le 1er numéro téléphone du candidat est requis"),
    body("telephone2").not().isEmpty().withMessage("le 2ème numéro téléphone du candidat est requis"),
    body("date_naissance").not().isEmpty().withMessage("la date de naissance du candidat est requise"),
    body("lieu_naissance").not().isEmpty().withMessage("le lieu de naissance du candidat est requis"),
    body("nombre_enfant").not().isEmpty().withMessage("le nombre d\'enfant du candidat est requis"),
    body("adresse").not().isEmpty().withMessage("l\'adresse du candidat est requise"),
    body("type_piece").not().isEmpty().withMessage("le type de pièce du candidat est requis"),
    body("numero_piece").not().isEmpty().withMessage("le numéro de la pièce du candidat est requis"),
    body("email").not().isEmpty().withMessage("l\'email du candidat est requis"),
    body("matricule").not().isEmpty().withMessage("le matricule du candidat est requis"),
    body("date_prise_service").not().isEmpty().withMessage("la date de prise de service est requise"),
    // body("photo").not().isEmpty().withMessage("la photo du candidat est requise"),
    Validator.validate,
    (req:any, res:any)=>{
        Cdg.api(res, FonctionnaireController.save(req.body))
})
route.get('/fonctionnaire/all',
    (req:any,res:any)=>{
        Cdg.api(res, FonctionnaireController.allFonctionnaire())
    })
route.get('/fonctionnaire/:id_fonctionnaire',
    param("id_fonctionnaire").isNumeric().withMessage("l'ID du fonctionnaire est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, FonctionnaireController.findById(req.params.id_fonctionnaire))
})
route.delete('/fonctionnaire/:id_fonctionnaire',
    param("id_fonctionnaire").isNumeric().withMessage("l'ID du fonctionnaire est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, FonctionnaireController.delete(req.params.id_fonctionnaire))
})
route.put('/fonctionnaire',
    body("genre").not().isEmpty().withMessage("le genre du candidat est requis"),
    body("civilite").not().isEmpty().withMessage("la civilité du candidat est requis"),
    body("situation_matrimoniale").not().isEmpty().withMessage("la situation matrimoniale du candidat est requis"),
    body("nom").not().isEmpty().withMessage("le nom du candidat est requis"),
    body("prenom").not().isEmpty().withMessage("le prénom du candidat est requis"),
    body("telephone1").not().isEmpty().withMessage("le 1er numéro téléphone du candidat est requis"),
    body("telephone2").not().isEmpty().withMessage("le 2ème numéro téléphone du candidat est requis"),
    body("date_naissance").not().isEmpty().withMessage("la date de naissance du candidat est requise"),
    body("lieu_naissance").not().isEmpty().withMessage("le lieu de naissance du candidat est requis"),
    body("nombre_enfant").not().isEmpty().withMessage("le nombre d\'enfant du candidat est requis"),
    body("adresse").not().isEmpty().withMessage("l\'adresse du candidat est requise"),
    body("type_piece").not().isEmpty().withMessage("le type de pièce du candidat est requis"),
    body("numero_piece").not().isEmpty().withMessage("le numéro de la pièce du candidat est requis"),
    body("email").not().isEmpty().withMessage("l\'email du candidat est requis"),
    body("matricule").not().isEmpty().withMessage("le matricule du candidat est requis"),
    body("date_prise_service").not().isEmpty().withMessage("la date de prise de service est requise"),
    body("photo").not().isEmpty().withMessage("la photo du candidat est requise"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, FonctionnaireController.update(req.body))
})

export class FonctionnaireRoute {
    @routeDecorator(route)
    static router : any
    constructor() {}
}