import { Cdg, color } from '../utils';
import {body, param} from 'express-validator';
import express, {Router, Response, NextFunction} from 'express';
import {routeDecorator} from './decorators.route';
import { EmploiController } from '../http/controllers';
import {Validator} from "../http/midlewares";

const route = Router();
(req : any, res : Response, next : NextFunction) => {
    return req.lang
};

route.post('/emploi/save',
    body("libelle_emploi").not().isEmpty().withMessage("le libellé de l'emploi est requis"),
    Validator.validate,
    (req:any, res:any)=>{
    Cdg.api(res, EmploiController.save(req.body))
})
route.get('/emploi/all',
    (req:any,res:any)=>{
        Cdg.api(res, EmploiController.allEmploi())
})
route.get('/emploi/:id_emploi',
    param("id_emploi").isNumeric().withMessage("l'ID de l'emploi est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, EmploiController.findById(req.params.id_emploi))
})
route.delete('/emploi/:id_emploi',
    param("id_emploi").isNumeric().withMessage("l'ID de l'emploi est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, EmploiController.delete(req.params.id_emploi))
})
route.put('/emploi',
    body("id").isNumeric().withMessage("l'ID de l'emploi est requis"),
    body("libelle_emploi").not().isEmpty().withMessage("le libellé de l'emploi est requis"),
    Validator.validate,
    (req:any,res:any)=>{
        Cdg.api(res, EmploiController.update(req.body))
})

export class EmploiRoute {
    @routeDecorator(route)
    static router : any
    constructor() {}
}