
const FPDF= require('node-fpdf')
export class PdfHelper{
    static generateInscTicket(data: any){
        const pdf = new FPDF('P','mm','A4')
                pdf.AddPage();
                pdf.Image('assets/page1.jpg',0,0,210);
                pdf.SetFont('Arial','B',6);
                // infos personnelles
                /* *********************************************************************************************** */
                pdf.SetXY(50.6,65);pdf.Cell(35,3,"num_insc",0,0,"L");
                pdf.SetXY(50.6,69);pdf.Cell(35,3,"nom_prenom",0,0,"L");
                pdf.SetXY(50.6,72.2);pdf.Cell(35,3,"Date lieu nais",0,0,"L");
                pdf.SetXY(50.6,75.7);pdf.Cell(35,3,"Matrimoniale",0,0,"L");
                pdf.SetXY(50.6,79.7);pdf.Cell(35,3,"adr postale",0,0,"L");
                pdf.SetXY(50.6,83);pdf.Cell(35,3,"adr email",0,0,"L");
                pdf.SetXY(50.6,86.5);pdf.Cell(35,3,"Nat piece",0,0,"L");
                pdf.SetXY(50.6,90.5);pdf.Cell(35,3,"num piece",0,0,"L");
                pdf.SetXY(50.6,94.5);pdf.Cell(35,3," place residence",0,0,"L");
                pdf.SetXY(50.6,99);pdf.Cell(35,3,"Nom_pere",0,0,"L");
                pdf.SetXY(50.6,103);pdf.Cell(35,3,"Nom_mere",0,0,"L");
                //**************************************************************** */
                /** infos concours */
                pdf.SetXY(50.6,129.5);pdf.Cell(35,3,"type_concours",0,0,"L");
                pdf.SetXY(50.6,133.5);pdf.Cell(35,3,"libelle_concours",0,0,"L");
                pdf.SetXY(50.6,137);pdf.Cell(35,3,"libelle_diplome",0,0,"L");
                pdf.SetXY(50.6,140.2);pdf.Cell(35,3,"Ville_compo",0,0,"L");
                pdf.SetXY(50.6,143.7);pdf.Cell(35,3,"date_compo",0,0,"L");
                pdf.SetXY(125.6,143.8);pdf.Cell(35,3,"Montant_compo",0,0,"L");
                return pdf.Output('base64',`test.pdf`);
    }
}