
export const color ={
    Reset :"\x1b[0",
    Bright :"\x1b[1",
    Dim :"\x1b[2",
    Underscore :"\x1b[4",
    Blink :"\x1b[5",
    Reverse :"\x1b[7",
    Hidden :"\x1b[8",

    FgBlack :"\x1b[30",
    FgRed :"\x1b[31",
    FgGreen :"\x1b[32",
    FgYellow :"\x1b[33",
    FgBlue :"\x1b[34",
    FgMagenta :"\x1b[35",
    FgCyan :"\x1b[36",
    FgWhite :"\x1b[37",

    BgBlack :"\x1b[40",
    BgRed :"\x1b[41",
    BgGreen :"\x1b[42",
    BgYellow :"\x1b[43",
    BgBlue :"\x1b[44",
    BgMagenta :"\x1b[45",
    BgCyan :"\x1b[46",
    BgWhite :"\x1b[47",
}