import {Request, Response} from 'express'
import crypto from "crypto";
export class Cdg{
    static api(context : Response, promise : Promise < any >){
        promise.then((res : any) => {
            let status = res.status;
            let message = res.message;
            let data = res.data;
            let response = {
                message: message,
                data: (data !== undefined || true ? data : [])
            };

            return context.status(status).json(response);
        }).catch((err : Error) => {
            return context.status(500).json({error: true, message: "une erreur interne s'est produite veuillez réssayer plutard", errMsg: err});
        })
    }
    static cryptPassword(pwd:string):string{
        let crypt = crypto.createHash("sha256")
        return crypt.update(pwd).digest('hex')
    }
    static test(text:string){
        console.log(text)
    }
}