import jwt from 'jsonwebtoken'
import {NextFunction, Request, Response} from "express";
import dotenv from 'dotenv'
dotenv.config()
export class AuthenticationHlp{
    private static secret:string = process.env.JWT_KEY_SECRET!
    static createToken(user:{ login:string}){
        return jwt.sign(user, this.secret, { expiresIn: '120m' });
    }
    static verifyAuth(req:Request, res:Response, next: NextFunction) {
        const authHeader = req.headers['authorization']

        const token = authHeader && authHeader.split(' ')[1]

        if (token == null) return res.status(401).json({message:"action non authorisée", data: null})

        jwt.verify(token, process.env.JWT_KEY_SECRET as string, (err: any, user: any) => {
            console.log(err)
            if (err) return res.status(403).json({message:"token invalide", data: null})
            //@ts-ignore
            req.user = user
            next()
        })
    }
}