import { color } from "../../utils";
import {NiveauModel} from "../../models";

export class NiveauController{

    static save(data: any){
        return new Promise(async (resolve, reject) => {
            try{
                const result = await NiveauModel.create(data)
                resolve({status: 200, message: "Données valides", data:result})
                // @ts-ignore
            }catch(err:Error){
                reject({status:500, error: true, errorMessage: err})
            }
        })
    }

    static allNiveau(){
        return new Promise(async (resolve, reject)=>{
            try {
                const niveau = await NiveauModel.findAll();
                const message = `Liste des niveaux diplômes`;
                resolve({status: 200, message, data: niveau})
                // @ts-ignore
            }catch (err: Error) {
                const message = `Aucun niveau trouvé dans la base de données`;
                resolve({status: 500, message, data: err})
            }
        })
    }

    static findById(data:any){
        return new Promise(async(resolve, reject)=>{
            const id = parseInt(data);

            try {
                const niveau = await NiveauModel.findOne({
                    where: id
                });
                const message = `Le niveau ${niveau.libelle_niveau} a été trouvé`;
                resolve({status: 200, message, data: niveau})
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucun niveau dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

    static delete(data:any){
        return new Promise(async(resolve, reject) => {
            const id = parseInt(data)

            try {
                let niveau =await NiveauModel.findOne({
                    where: id
                });
                niveau.destroy()
                const message = `Le niveau a été supprimé`;
                resolve({status: 200, message, data: niveau});
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucun niveau dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

    static update(data:any) {
        return new Promise(async(resolve, reject) => {
            const id = parseInt(data.id)

            try {
                let niveau =await NiveauModel.findOne({
                    where: id
                });
                niveau.update(data)
                const message = `Le niveau a été mis à jour`;
                resolve({status: 200, message, data: niveau});
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucun niveau dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

}