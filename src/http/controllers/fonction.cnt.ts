import { color } from "../../utils";
import { FonctionModel } from "../../models";

export class FonctionController{

    static save(data: any){
        return new Promise(async (resolve, reject) => {
            try{
                const result = await FonctionModel.create(data)
                resolve({status: 200, message: "Données valides", data:result})
                // @ts-ignore
            }catch(err:Error){
                reject({status:500, error: true, errorMessage: err})
            }
        })
    }

    static allFonction(){
        return new Promise(async (resolve, reject)=>{
            try {
                const fonction = await FonctionModel.findAll();
                const message = `Liste des Fonctions`;
                resolve({status: 200, message, data: fonction})
                // @ts-ignore
            }catch (err: Error) {
                const message = `Aucune fonction trouvée dans la base de données`;
                resolve({status: 500, message, data: err})
            }
        })
    }

    static findById(data:any){
        return new Promise(async(resolve, reject)=>{
            const id = parseInt(data);

            try {
                const fonction = await FonctionModel.findOne({
                    where: id
                });
                const message = `La fonction ${fonction.libelle_fonction} a été trouvée`;
                resolve({status: 200, message, data: fonction})
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucune fonction dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

    static delete(data:any){
        return new Promise(async(resolve, reject) => {
            const id = parseInt(data)

            try {
                let fonction =await FonctionModel.findOne({
                    where: id
                });
                fonction.destroy()
                const message = `La fonction a été supprimée`;
                resolve({status: 200, message, data: fonction});
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucune fonction dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

    static update(data:any) {
        return new Promise(async(resolve, reject) => {
            const id = parseInt(data.id)

            try {
                let fonction =await FonctionModel.findOne({
                    where: id
                });
                fonction.update(data)
                const message = `La fonction a été mise à jour`;
                resolve({status: 200, message, data: fonction});
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucune fonction dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

}