import { color } from "../../utils";
import {DiplomeModel} from "../../models";

export class DiplomeController{

    static save(data: any){
        return new Promise(async (resolve, reject) => {
            try{
                const result = await DiplomeModel.create(data)
                resolve({status: 200, message: "Données valides", data:result})
                // @ts-ignore
            }catch(err:Error){
                reject({status:500, error: true, errorMessage: err})
            }
        })
    }

    static allDiplome(){
        return new Promise(async (resolve, reject)=>{
            try {
                const diplome = await DiplomeModel.findAll();
                const message = `Liste des Diplômes`;
                resolve({status: 200, message, data: diplome})
                // @ts-ignore
            }catch (err: Error) {
                const message = `Aucun diplôme trouvé dans la base de données`;
                resolve({status: 500, message, data: err})
            }
        })
    }

    static findById(data:any){
        return new Promise(async(resolve, reject)=>{
            const id = parseInt(data);

            try {
                const diplome = await DiplomeModel.findOne({
                    where: id
                });
                const message = `Le diplôme ${diplome.libelle_diplome} a été trouvé`;
                resolve({status: 200, message, data: diplome})
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucun diplôme dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

    static delete(data:any){
        return new Promise(async(resolve, reject) => {
            const id = parseInt(data)

            try {
                let diplome =await DiplomeModel.findOne({
                    where: id
                });
                diplome.destroy()
                const message = `Le diplôme a été supprimé`;
                resolve({status: 200, message, data: diplome});
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucun diplôme dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

    static update(data:any) {
        return new Promise(async(resolve, reject) => {
            const id = parseInt(data.id)

            try {
                let diplome =await DiplomeModel.findOne({
                    where: id
                });
                diplome.update(data)
                const message = `Le diplôme a été mis à jour`;
                resolve({status: 200, message, data: diplome});
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucun diplôme dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

}