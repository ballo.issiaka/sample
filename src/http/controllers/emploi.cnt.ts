import { color } from "../../utils";
import { EmploiModel } from "../../models";

export class EmploiController{

    static save(data: any){
        return new Promise(async (resolve, reject) => {
            try{
                const result = await EmploiModel.create(data)
                resolve({status: 200, message: "Données valides", data:result})
                // @ts-ignore
            }catch(err:Error){
                reject({status:500, error: true, errorMessage: err})
            }
        })
    }

    static allEmploi(){
        return new Promise(async (resolve, reject)=>{
            try {
                const emploi = await EmploiModel.findAll();
                const message = `Liste des Emplois`;
                resolve({status: 200, message, data: emploi})
                // @ts-ignore
            }catch (err: Error) {
                const message = `Aucun emploi trouvé dans la base de données`;
                resolve({status: 500, message, data: err})
            }
        })
    }

    static findById(data:any){
        return new Promise(async(resolve, reject)=>{
            const id = parseInt(data);

            try {
                const emploi = await EmploiModel.findOne({
                    where: id
                });
                const message = `L'emploi ${emploi.libelle_emploi} a été trouvé`;
                resolve({status: 200, message, data: emploi})
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucun emploi dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

    static delete(data:any){
        return new Promise(async(resolve, reject) => {
            const id = parseInt(data)

            try {
                let emploi =await EmploiModel.findOne({
                    where: id
                });
                emploi.destroy()
                const message = `L'emploi a été supprimé`;
                resolve({status: 200, message, data: emploi});
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucun emploi dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

    static update(data:any) {
        return new Promise(async(resolve, reject) => {
            const id = parseInt(data.id)

            try {
                let emploi =await EmploiModel.findOne({
                    where: id
                });
                emploi.update(data)
                const message = `L'emploi a été mis à jour`;
                resolve({status: 200, message, data: emploi});
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucun emploi dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

}