import {Cdg, color} from "../../utils"
import {CandidatDiplomeModel, CandidatModel} from "../../models"
import {AuthenticationHlp} from "../midlewares/authentication.hlp"
import { PdfHelper } from "../../helpers/pdf.hlp"

export class CandidatController{
    static pdfTest(){
        return new Promise<any>(async(resolve, reject) => {
            try{
                resolve({status: 200, message: "Données valides", data: PdfHelper.generateInscTicket(null)})
                // @ts-ignore
            }catch(err:Error){
                console.error(err)
                reject({status:500, error: true, errorMessage: err})
            }
        })
    }

    static save(data: any){
        return new Promise(async (resolve, reject) => {
            try{
                data.password = Cdg.cryptPassword(data.password)
                const result = await CandidatModel.create(data)

                let user = {...result.dataValues}
                delete user.password
                delete user.id_candidat
                resolve({status: 200, message: "Données valides", data:user})
                // @ts-ignore
            }catch(err:Error){
                reject({status:500, error: true, errorMessage: err})
            }
        })
    }
    static addDiplome(data:any){
        return new Promise(async (resolve, reject)=>{
            try{
                const candidat = parseInt(data.candidat)
                const diplome = parseInt(data.diplome)
                CandidatDiplomeModel.create(candidat, diplome, data.date)
                const user = await CandidatModel.findOne({
                    where: candidat
                });
                resolve({status: 200, message: "Données valides", data:user})
                // @ts-ignore
            }catch(err:Error){
                reject({status:500, error: true, errorMessage: err})
            }
        })
    }

    static allCandidats(){
        return new Promise(async (resolve, reject)=>{
            try {
                const candidat = await CandidatModel.findAll();
                const message = `Liste des Candidats`;
                resolve({status: 200, message, data: candidat})
                // @ts-ignore
            }catch (err: Error) {
                const message = `Aucun candidat trouvé dans la base de données`;
                resolve({status: 500, message, data: err})
            }
        })
    }

    static findById(data:any){
        return new Promise(async(resolve, reject)=>{
            const id = parseInt(data);

            try {
                const candidat = await CandidatModel.findOne({
                    where: id
                });
                console.log(candidat)
                const message = `Le candidat a été trouvé`;
                resolve({status: 200, message, data: candidat})
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucun candidat dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

    static delete(data:any){
        return new Promise(async(resolve, reject) => {
            const id = parseInt(data)

            try {
                let candidat =await CandidatModel.findOne({
                    where: id
                });
                candidat.destroy()
                const message = `Le candidat a été supprimé`;
                resolve({status: 200, message, data: candidat});
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucun candidat dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

    static update(data:any) {
        return new Promise(async(resolve, reject) => {
            const login = data.login

            try {
                let candidat =await CandidatModel.findOne({
                    where: {login}
                });
                candidat.update(data)
                const message = `Le candidat a été mis à jour`;
                resolve({status: 200, message, data: candidat});
                //@ts-ignore
            }catch (err: Error) {
                const message = ` ne correspond à aucun candidat dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

    static auth(data:{login:string, password:string}): Promise<any>{
        return new Promise(async(resolve, reject)=>{
            try{
                let login:string = data.login
                let candidat =await CandidatModel.findOne({
                    where: {login}
                })
                if(candidat){
                    if(candidat.password === Cdg.cryptPassword(data.password)){
                        console.log('mot de passe correcte')
                        let token = AuthenticationHlp.createToken({login:login})
                        let user = {...candidat.dataValues}
                        user.jwt = token
                        delete user.password
                        delete user.id_candidat
                        resolve({status: 200, message: "debug", data: user})
                    }else{
                        resolve({status: 422, message: "mot de passe incorrecte", data: null})
                    }
                }else{
                    resolve({status: 422, message: "login incorrect", data: null})
                }
            }//@ts-ignore
            catch(e: Error){
                const message = `une erreur est survenue`;
                console.error(e)
                reject({status: 500, message, data: e});
            }
        })
    }

}