import { color } from "../../utils";
import { TypeConcoursModel } from "../../models";

export class TypeConcoursController{

    static save(data: any){
        return new Promise(async (resolve, reject) => {
            try{
                const result = await TypeConcoursModel.create(data)
                resolve({status: 200, message: "Données valides", data:result})
                // @ts-ignore
            }catch(err:Error){
                reject({status:500, error: true, errorMessage: err})
            }
        })
    }

    static allTypeConcours(){
        return new Promise(async (resolve, reject)=>{
            try {
                const typeconcours = await TypeConcoursModel.findAll();
                const message = `Liste des Types de Concours`;
                resolve({status: 200, message, data: typeconcours})
                // @ts-ignore
            }catch (err: Error) {
                const message = `Aucun Type de Concours trouvé dans la base de données`;
                resolve({status: 500, message, data: err})
            }
        })
    }

    static findById(data:any){
        return new Promise(async(resolve, reject)=>{
            const id = parseInt(data);

            try {
                const typeconcours = await TypeConcoursModel.findOne({
                    where: id
                });
                const message = `Le type de concours ${typeconcours.libelle_type_concours} a été trouvé`;
                resolve({status: 200, message, data: typeconcours})
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucun Type de Concours dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

    static delete(data:any){
        return new Promise(async(resolve, reject) => {
            const id = parseInt(data)

            try {
                let typeconcours =await TypeConcoursModel.findOne({
                    where: id
                });
                typeconcours.destroy()
                const message = `Le Type de Concours a été supprimé`;
                resolve({status: 200, message, data: typeconcours});
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucun Type de Concours dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

    static update(data:any) {
        return new Promise(async(resolve, reject) => {
            const id = parseInt(data.id)

            try {
                let typeconcours =await TypeConcoursModel.findOne({
                    where: id
                });
                typeconcours.update(data)
                const message = `Le Type de Concours a été mis à jour`;
                resolve({status: 200, message, data: typeconcours});
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucun emploi dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

}