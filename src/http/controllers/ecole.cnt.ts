import { color } from "../../utils";
import {EcoleModel} from "../../models";

export class EcoleController{

    static save(data: any){
        return new Promise(async (resolve, reject) => {
            try{
                const result = await EcoleModel.create(data)
                resolve({status: 200, message: "Données valides", data:result})
                // @ts-ignore
            }catch(err:Error){
                reject({status:500, error: true, errorMessage: err})
            }
        })
    }

    static allEcole(){
        return new Promise(async (resolve, reject)=>{
            try {
                const ecole = await EcoleModel.findAll();
                const message = `Liste des Écoles`;
                resolve({status: 200, message, data: ecole})
                // @ts-ignore
            }catch (err: Error) {
                const message = `Aucune école trouvée dans la base de données`;
                resolve({status: 500, message, data: err})
            }
        })
    }

    static findById(data:any){
        return new Promise(async(resolve, reject)=>{
            const id = parseInt(data);

            try {
                const ecole = await EcoleModel.findOne({
                    where: id
                });
                const message = `L'école ${ecole.libelle_ecole} a été trouvé`;
                resolve({status: 200, message, data: ecole})
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucune école dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

    static delete(data:any){
        return new Promise(async(resolve, reject) => {
            const id = parseInt(data)

            try {
                let ecole =await EcoleModel.findOne({
                    where: id
                });
                ecole.destroy()
                const message = `L'école a été supprimé`;
                resolve({status: 200, message, data: ecole});
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucune école dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

    static update(data:any) {
        return new Promise(async(resolve, reject) => {
            const id = parseInt(data.id)

            try {
                let ecole =await EcoleModel.findOne({
                    where: id
                });
                ecole.update(data)
                const message = `L'école a été mise à jour`;
                resolve({status: 200, message, data: ecole});
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucune école dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

}