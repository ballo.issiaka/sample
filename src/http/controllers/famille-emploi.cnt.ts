import { color } from "../../utils";
import {FamilleEmploiModel} from "../../models";

export class FamilleEmploiController{

    static save(data: any){
        return new Promise(async (resolve, reject) => {
            try{
                const result = await FamilleEmploiModel.create(data)
                resolve({status: 200, message: "Enregistrement effectué avec succès", data:result})
                // @ts-ignore
            }catch(err:Error){
                reject({status:500, error: true, errorMessage: err})
            }
        })
    }

    static all(){
        return new Promise(async (resolve, reject)=>{
            try {
                const ecole = await FamilleEmploiModel.findAll();
                const message = `ok`;
                resolve({status: 200, message, data: ecole})
                // @ts-ignore
            }catch (err: Error) {
                const message = `Auncune correspondance trouvée dans la base de données`;
                resolve({status: 500, message, data: err})
            }
        })
    }

    static findById(data:any){
        return new Promise(async(resolve, reject)=>{
            const id = parseInt(data);

            try {
                const ecole = await FamilleEmploiModel.findOne({
                    where: id
                });
                const message = `ok`;
                resolve({status: 200, message, data: ecole})
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucune école dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

    static delete(data:any){
        return new Promise(async(resolve, reject) => {
            const id = parseInt(data)

            try {
                let ecole =await FamilleEmploiModel.findOne({
                    where: id
                });
                ecole.destroy()
                const message = `Suppression éffectuée`;
                resolve({status: 200, message, data: ecole});
                //@ts-ignore
            }catch (err: Error) {
                const message = `Auncune correspondance trouvée dans la base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

    static update(data:any) {
        return new Promise(async(resolve, reject) => {
            const id = parseInt(data.id_famille_emploi)

            try {
                let ecole =await FamilleEmploiModel.findOne({
                    where: id
                });
                ecole.update(data)
                const message = `Mise à jour éffectuée avec succès`;
                resolve({status: 200, message, data: ecole});
                //@ts-ignore
            }catch (err: Error) {
                const message = `Auncune correspondance trouvée dans la base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

}