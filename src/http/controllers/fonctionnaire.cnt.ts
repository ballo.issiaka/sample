import { color } from "../../utils";
import {FonctionnaireModel} from "../../models";

export class FonctionnaireController{

    static save(data: any){
        return new Promise(async (resolve, reject) => {
            try{
                const result = await FonctionnaireModel.create(data)
                resolve({status: 200, message: "Données valides", data:result})
                // @ts-ignore
            }catch(err:Error){
                reject({status:500, error: true, errorMessage: err})
            }
        })
    }

    static allFonctionnaire(){
        return new Promise(async (resolve, reject)=>{
            try {
                const fonctionnaire = await FonctionnaireModel.findAll();
                const message = `Liste des Fonctionnaires`;
                resolve({status: 200, message, data: fonctionnaire})
                // @ts-ignore
            }catch (err: Error) {
                const message = `Aucun fonctionnaire trouvé dans la base de données`;
                resolve({status: 500, message, data: err})
            }
        })
    }

    static findById(data:any){
        return new Promise(async(resolve, reject)=>{
            const id = parseInt(data);

            try {
                const fonctionnaire = await FonctionnaireModel.findOne({
                    where: id
                });
                const message = `Le fonctionnaire a été trouvé`;
                resolve({status: 200, message, data: fonctionnaire})
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucun fonctionnaire dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

    static delete(data:any){
        return new Promise(async(resolve, reject) => {
            const id = parseInt(data)

            try {
                let fonctionnaire =await FonctionnaireModel.findOne({
                    where: id
                });
                fonctionnaire.destroy()
                const message = `Le fonctionnaire a été supprimé`;
                resolve({status: 200, message, data: fonctionnaire});
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucun fonctionnaire dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

    static update(data:any) {
        return new Promise(async(resolve, reject) => {
            const id = parseInt(data.id)

            try {
                let fonctionnaire =await FonctionnaireModel.findOne({
                    where: id
                });
                fonctionnaire.update(data)
                const message = `Le fonctionnaire a été mis à jour`;
                resolve({status: 200, message, data: fonctionnaire});
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucun fonctionnaire dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

}