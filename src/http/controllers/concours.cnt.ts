import { color } from "../../utils";
import {ConcoursModel} from "../../models";

export class ConcoursController{

    static save(data: any){
        return new Promise(async (resolve, reject) => {
            try{
                const result = await ConcoursModel.create(data)
                resolve({status: 200, message: "Données valides", data:result})
                // @ts-ignore
            }catch(err:Error){
                reject({status:500, error: true, errorMessage: err})
            }
        })
    }

    static allConcours(){
        return new Promise(async (resolve, reject)=>{
            try {
                const concours = await ConcoursModel.findAll();
                const message = `Liste des Concours`;
                resolve({status: 200, message, data: concours})
                // @ts-ignore
            }catch (err: Error) {
                const message = `Aucun concours trouvé dans la base de données`;
                resolve({status: 500, message, data: err})
            }
        })
    }

    static findById(data:any){
        return new Promise(async(resolve, reject)=>{
            const id = parseInt(data);

            try {
                const concours = await ConcoursModel.findOne({
                    where: id
                });
                const message = `Le concours ${concours.libelle_concours} a été trouvé`;
                resolve({status: 200, message, data: concours})
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucun concours dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

    static delete(data:any){
        return new Promise(async(resolve, reject) => {
            const id = parseInt(data)

            try {
                let concours =await ConcoursModel.findOne({
                    where: id
                });
                concours.destroy()
                const message = `Le concours a été supprimé`;
                resolve({status: 200, message, data: concours});
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucun concours dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

    static update(data:any) {
        return new Promise(async(resolve, reject) => {
            const id = parseInt(data.id)

            try {
                let concours =await ConcoursModel.findOne({
                    where: id
                });
                concours.update(data)
                const message = `Le concours a été mis à jour`;
                resolve({status: 200, message, data: concours});
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucun concours dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

}