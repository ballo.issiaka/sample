import { color } from "../../utils";
import { TypeEnseignementModel } from "../../models";

export class TypeEnseignementController{

    static save(data: any){
        return new Promise(async (resolve, reject) => {
            try{
                const result = await TypeEnseignementModel.create(data)
                resolve({status: 200, message: "Données valides", data:result})
                // @ts-ignore
            }catch(err:Error){
                reject({status:500, error: true, errorMessage: err})
            }
        })
    }

    static allTypeEnseignement(){
        return new Promise(async (resolve, reject)=>{
            try {
                const typeenseignement = await TypeEnseignementModel.findAll();
                const message = `Liste des Types d'Enseignements`;
                resolve({status: 200, message, data: typeenseignement})
                // @ts-ignore
            }catch (err: Error) {
                const message = `Aucun Type d'Enseignement trouvé dans la base de données`;
                resolve({status: 500, message, data: err})
            }
        })
    }

    static findById(data:any){
        return new Promise(async(resolve, reject)=>{
            const id = parseInt(data);

            try {
                const typeenseignement = await TypeEnseignementModel.findOne({
                    where: id
                });
                const message = `Le type d'Enseignement ${typeenseignement.libelle_enseignement} a été trouvé`;
                resolve({status: 200, message, data: typeenseignement})
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucun Type d'Enseignement dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

    static delete(data:any){
        return new Promise(async(resolve, reject) => {
            const id = parseInt(data)

            try {
                let typeenseignement =await TypeEnseignementModel.findOne({
                    where: id
                });
                typeenseignement.destroy()
                const message = `Le Type d'Enseignement a été supprimé`;
                resolve({status: 200, message, data: typeenseignement});
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucun Type d'Enseignement dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

    static update(data:any) {
        return new Promise(async(resolve, reject) => {
            const id = parseInt(data.id)

            try {
                let typeenseignement =await TypeEnseignementModel.findOne({
                    where: id
                });
                typeenseignement.update(data)
                const message = `Le Type d'Enseignement a été mis à jour`;
                resolve({status: 200, message, data: typeenseignement});
                //@ts-ignore
            }catch (err: Error) {
                const message = `L'ID ${id} ne correspond à aucun Type d'Enseignement dans la Base de données`;
                reject({status: 500, message, data: err});
            }
        })
    }

}